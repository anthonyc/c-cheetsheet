/**
 * @file 2_pointers.cpp
 * @author Anthony Correia (anthony.correia@cern.ch)
 * @brief 
 * @version 0.1
 * @date 2022-12-04
 * 
 * 
 */

#include <iostream>
using namespace std;

/** @addtogroup basics 
 *  @{
 */

/////////////////////////////////////////////////////////////////////////////
/// @defgroup pointers Pointers
/// @{
/////////////////////////////////////////////////////////////////////////////

/*********************************************************************
 *  @defgroup definition Definition
 *  @{
 ********************************************************************/
/// @brief Some variable (integer)
int variable = 4;

/// @brief Define a pointer by an address of a variable
int* address = &variable;

/// @brief Dereference the pointer (access the pointer value)
int pointer_value = *address;

/// @brief Pointer to nothing (`nullptr`).
/// 
/// Use cases: 
/// - `*nothing_pointer` will throw a "segmentation fault"
/// - If `void* nothing_pointer` is used, `nothing_pointer` points to a random
///   address
/// 
/// Don't use `NULL` (which is equivalent to 0) as it won't throw an error
/// if `nothing_pointer` is not a pointer.
void* nothing_pointer = nullptr;
/// @brief Pointer to nothing (`nullptr`), with a type (`int`)
int* nothing_pointer_int = nullptr;

/// @}
/*********************************************************************
 *  @defgroup arrays Arrays
 * CAUTION: if `array1[4]` is outside the list, there are 2 possibilities
 *     - segmentation fault: software has attempted to attempted to access
 *       a restricted area of memory
 *     - it accesses a memory address available and returns
 *       a random number
 *  @{
 ********************************************************************/
/// @brief A simple array
int array1[3] = {1, 2, 3};
/// @brief A simple array, without defining its size
/// (the compiler is smart enough)
int array2[] = {1, 2, 3};

/// @brief An array is just a pointer to the 1st element
int* first_value_array = array1;
/// @brief To access to the next element of an array, increment the
///        memory address
int* second_value_array = array1 + 1;

/// @}
/// @}
/// @}

int main() {
    cout << "variable: " << variable << endl;
    cout << "Address: " << address << endl;
    cout << "Pointer value: " << pointer_value << endl;
    cout << "Array = pointer to 1st element: " << array1 << endl;
    cout << "Array\'s first value: " << *array1 << endl;
    cout << "Array\'s second value: " << *(array1 + 1) << endl;

    return 0;
}
