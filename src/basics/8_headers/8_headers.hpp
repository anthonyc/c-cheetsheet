

/** @addtogroup basics 
 *  @{
 */
#include <cstdint>



/////////////////////////////////////////////////////////////////////////////
/// @addtogroup preprocessors Preprocessors
/// Include headers or
/// customisation for specific compilers or platforms. \n
/// A header (`.hpp`) contain a set of declarations defining some functionality.
/// The implementation is then defined somewhere else.
/// @{
/////////////////////////////////////////////////////////////////////////////

// Macro constant
#define PI 3.14

// Function-style macro
#define CHECK_ERROR(x) if ((x) != PI) \
    std::cerr << #x " was not pi\n";

// Compile time configuration
#if defined(USE64BIT)
    using myint = uint64_t;
#else
    using myint = uint32_t;
#endif

// Gards in case of multiple (transitive) inclusions of a headers
// to avoid to define new names multiple times (compile error)
#ifndef MY_HEADER_INCLUDED
#define MY_HEADER_INCLUDED
    using myint2 = uint32_t;
#endif
// Non-standard equivalent
#pragma once
    using myint3 = uint32_t;
    

/// @brief Print "hello"
void printHello();

/// @}
/////////////////////////////////////////////////////////////////////////////
/// @defgroup inline Inline keyword
/// Linker merges all occurences into one.
/// This allows to define global constants in headers included multiple
/// times. \n
/// NB: avoid inline global variables
/// @{
/////////////////////////////////////////////////////////////////////////////

/// @brief An inline constant variable (integer) defined in a header.
inline const int a = 0;

/// @}