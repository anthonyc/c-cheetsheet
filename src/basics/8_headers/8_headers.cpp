/**
 * @file 8_headers.cpp
 * @author Anthony Correia (anthony.correia@cern.ch)
 * @brief 
 * @version 0.1
 * @date 2022-12-04
 * 
 * 
 */

/** @addtogroup basics 
 *  @{
 */


/////////////////////////////////////////////////////////////////////////////
/// @defgroup preprocessors Preprocessors
/// @{
/////////////////////////////////////////////////////////////////////////////

// include a standard library
#include <iostream>

// header file inclusion
#include "8_headers.hpp"

/// @short Use type `myint` defined in `8_headers.hpp`.
myint a = 12;

void printHello() {
    std::cout << "hello" << std::endl;
}

/// @} @}

int main() {
    printHello();
    return 0;
}
