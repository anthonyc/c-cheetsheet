/**
 * @file 3_structures.cpp
 * @author Anthony Correia (anthony.correia@cern.ch)
 * @brief 
 * @version 0.1
 * @date 2022-12-04
 * 
 * 
 */

#include <iostream>
using namespace std;

/** @addtogroup basics 
 *  @{
 */

/////////////////////////////////////////////////////////////////////////////
/// @defgroup structures Structures
/// @{
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
/// @defgroup namespaces Namespaces
/// Allows to segment code to avoid name clashes
/// @{
/////////////////////////////////////////////////////////////////////////////

/// @brief Some variable (integer)
int var = 1;

/// @brief Definition of a namespace that contains an embedded namespace
namespace mynamespace {
    int var = 2;
    /// @brief Define an embedded namespace
    namespace inner {
        int var2 = 3;
        // refer to global scope
        int copy_var = ::var;
    }
}

/// @brief Access variable in the namespace
int var_in_namespace = mynamespace::var;

/// @brief Access variable in the nested namespace
int var_in_inner_namespace = mynamespace::inner::var2;

// Make the variables available without `::` separator
using namespace mynamespace::inner;
/// @brief Access the variable defined in a namespace and made available
///        using `using namespace`
int var_in_inner_namespace_available = var2;

// Re-open a namespace
namespace mynamespace {
    int var3 = 0;
}

// Define (and re-open) a nested namespace [C++ 17]
namespace mynamespace::inner {
    int inner_var_reopen = 5;
    // (available as `using namespace mynamespace::inner`
    // was used earlier)
}

/// @brief Namespace whose name is generated automatically
///
/// `unnamed_namespace_var` is available globally but not in other `cpp` files.
/// This is equivalent to the deprecated `static int local_var;`
namespace {
    int unnamed_namespace_var = 6;
}


void namespaces() {
    cout << "var: " << var << endl;
    cout << "var in mynamespace: " << var_in_namespace << endl;
    cout << "var in inner mynamespace: " << var_in_inner_namespace << endl;    
    cout << "var in inner mynamespace made available: "
         << var_in_inner_namespace_available
         << endl;
    cout << "Copied var referring to global scope: "
         << copy_var
         << endl;
    cout << "var3 from reopened namespace: "
         << mynamespace::var3
         << endl;
    cout << "inner var from reopened namespace: "
         << inner_var_reopen
         << endl;
    cout << "Variable in unnamed namespace: "
         << unnamed_namespace_var
         << endl;
}

/// @}
/////////////////////////////////////////////////////////////////////////////
/// @defgroup structs Structures
/// A class that only has public members (called qualifiers) \n
/// NB: From C+17, rather use std::variant
/// @{
/////////////////////////////////////////////////////////////////////////////

/// @short Define a structure
struct INDIVIDUAL {
    /// @brief 8-bit `int`
    unsigned char age;
    /// @brief 32-bit `float`
    float weight;
}; // don't forget the ;

/// @brief Use of a structure
/// 
/// Memory layout \n
///        :  pointer to first line 0x3000 \n
/// weight :  <- 70.  -> # 0x300C \n
/// age    :  45 _  _  _ # 0x3008 \n
/// weight :  <- 78.5 -> # 0x3004 \n
/// age    :  22 _  _  _ # 0x3000 \n
void structs() {
    /// @brief Instantiation of a structure
    INDIVIDUAL student;
    student.age = 22;
    student.weight = 78.5f;

    /// @brief Instantiation of a structure using an array
    INDIVIDUAL teacher = {
        45, 70.f
    };    
}

/// @}
/////////////////////////////////////////////////////////////////////////////
/// @defgroup unions Unions
/// Members packed together at same memory location
/// @{
/////////////////////////////////////////////////////////////////////////////

/// @short Define an union
union Duration {
    /// @brief 32-bit
    int seconds;
    /// @brief 16-bit
    short hours;
    /// @brief 8-bit
    char days;
}; // don't forget ;


/// @brief Use of an union
/// 
/// Memory layout \n
///        :  pointer to first line 0x3000 \n
///       :  _    _      _     _  # 0x300C \n
/// d3    :  9    _      _     _  # 0x3008 \n
/// d2    :  <- 8 ->     _     _  # 0x3004 \n
/// d1    :  <-   237843  ->      # 0x3000
void unions() {
    // INSTANTIATION -------------------------------
    Duration d1, d2, d3;
    d1.seconds = 237843;
    d2.hours = 8;
    d3.hours = 9;
    /// @short change the active member of the union `d3`
    /// NB: `d3.seconds` and `d3.hours` are garbage
    d3.days = 2;
}

/// @}
/////////////////////////////////////////////////////////////////////////////
/// @defgroup enums Enums
/// List of related constants (enumerators)
/// which correspond to numbers, represented by names
/// @{
/////////////////////////////////////////////////////////////////////////////

/// @short Define an enumeration without specifying the corresponding integer
enum VehicleType {
    BIKE, // = 0
    CAR,  // = 1
    BUS   // = 2
};

/// @short Define an enumeration, by specifying the corresponding integer
enum AnimalType {
    CAT = 1,
    DOG = 7,
    PANDA = 2
};

/// @short Define an enumeration without specifying the corresponding integer
VehicleType vehicle = CAR;
AnimalType  animal = CAT;

void enums() {
    cout << "Vehicle: " << vehicle << endl;    
    cout << "Animal: " << animal << endl;    
}

/// @}
/////////////////////////////////////////////////////////////////////////////
/// @defgroup enum_classes enum class
/// `enum` with scope
///     - avoid name clases in enumerator names
///     - strong typing, avoid automatic conversion to `int`
/// @{
/////////////////////////////////////////////////////////////////////////////

/// @brief Define an `enum class`
enum class VType {Bus, Car};

/// @brief Instantiate an `enum class`
///
/// `t + 1` would raise an error (`t` is not an integer)
VType t = VType::Bus;

/// @}
/// @}
/// @}

int main() {
    namespaces();
    structs();
    enums();
    return 0;
}