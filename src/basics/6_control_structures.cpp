/**
 * @file 6_constrol_structures.cpp
 * @author Anthony Correia (anthony.correia@cern.ch)
 * @brief 
 * @version 0.1
 * @date 2022-12-04
 * 
 * 
 */

#include <iostream>
using namespace std;

/** @addtogroup basics 
 *  @{
 */


/////////////////////////////////////////////////////////////////////////////
/// @defgroup control Control structures
/// @{
/////////////////////////////////////////////////////////////////////////////

/*********************************************************************
 *  @defgroup cond Conditional structures
 *  @{
 ********************************************************************/

/// @brief Use of `if`, `else if` and `else`
/// @param a 
void if_operator(int a = 0) {
    if (a == 0){
        cout << "a is 0" << endl;
    } else if (a == 1) {
        cout << "a is 1" << endl;
    } else {
        cout << "a is" << a << endl;
    }
}

/// @brief Use of the conditional operator
///
/// `{condition} ? {expression1} : {expression2};` 
/// returns `expression1` if `condition` else `expression2` \n
/// NB: only use when obvious and not nested
/// @param a 
void conditional_operator(int a = 0) {
    string result = (a == 0) ? "a is 0" : "a is not 0";
    cout << result << endl;
}

/// @brief Use of the `switch` operator
/// 
/// `break` so that the execution does not full through the next case.
/// @param a 
void switch_operator(int a = 0) {
    switch(a) {
        case 0: cout << "a is 0" << endl; break;
        case 1: cout << "a is 1" << endl; break;
        case 2: [[fallthrough]]; // fall through to suppress C++17 warning
        default: cout << "a is neither 0 nor 1" <<endl; break; // may be omitted
    }
}

/// @brief "init-statement" inside an `if` or `switch` to limit scope. [C++17]
/// 
/// Usage: `if (initialisation; condition)`
/// or `switch (initialisation; condition)`
void init_inside_if(){
    if (int a = 1; a == 0){ // a define for the `if`-`else` structure
        cout << "a is 0" << endl;
    } else if (a == 1) {
        cout << "a is 1" << endl;
    } else {
        cout << "a is" << a << endl;
    }
    // a is no more accessible
}

/// @}
/*********************************************************************
 *  @defgroup loop Loop structures
 *  You should not use the `break`, `continue` and `goto`
 *  jump statements inside a loop.
 *  @{
 ********************************************************************/

/// @brief For loop
/// 
/// Usage: `for (initialisations; condition; increments)`
void for_loop(){
    for (int i = 0; i < 5; i++) {
        cout << i << " ";
    }
    cout << endl;
    for (int i = 0; i < 5; i++) cout << i << " "; // single statement
    cout << endl;
}

/// @brief Range-based loop [C++11]
/// 
/// Usage: `for (type iteration_variable: range)`
void range_based_loop(){
    for (int i : {1, 2, 3, 4}) cout << i << " ";
    cout << endl;
}

/// @brief While loop
/// 
/// Usage: `while (condition) {statements}` \n
/// Brases operational for single-statement body.
void while_loop(){
    int i = 0;
    while (i < 5) {
        cout << i;
        i++;
    }
    cout << endl;
}

/// @brief Do-while loop
/// 
/// Usage: `do {statements} while(condition)` \n
/// Brases operational for single-statement body.
void do_while_loop(){
    int i = 0;
    do {
        cout << i;
        i++;
    } while(i < 5);
    cout << endl;
}

/// @}
/// @}

int main () {
    if_operator();
    conditional_operator();
    switch_operator();
    init_inside_if();
    for_loop();
    range_based_loop();
    while_loop();
    do_while_loop();
    return 0;
}
