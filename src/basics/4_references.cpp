/**
 * @file 4_references.cpp
 * @author Anthony Correia (anthony.correia@cern.ch)
 * @brief 
 * @version 0.1
 * @date 2022-12-04
 * 
 * 
 */
#include <iostream>
using namespace std;


/// @brief A variable (integer)
int var = 2;

/** @addtogroup basics 
 *  @{
 */

/////////////////////////////////////////////////////////////////////////////
/// @defgroup references References
/// References allow for direct access to another object. \n
/// Use cases:
///     - shortcuts (better readability)
///     - Declared as a `const` allows to require only read access
///     - Parameters passed by reference in a function
/// @{
/////////////////////////////////////////////////////////////////////////////


/*********************************************************************
 *  @defgroup def Definition
 *  @{
 ********************************************************************/

/// @brief a reference to a variable
int &varref = var;

/// @brief a constant reference to a variable (cannot be modified)
///
/// (they must be used for temporary objects)
const int &const_varref = var;

/// @}
/*********************************************************************
 *  @defgroup change Change
 *  @{
 ********************************************************************/

/**
 * @brief Change #var to 3 by changing #varref
 * 
 */
void change_by_reference() {
    varref = 3;  // var is 3 as well
}

/// @}
/// @}
/// @}

int main() {
    cout << "Value: " << var << endl;
    cout << "Reference value: " << varref << endl;
    change_by_reference();    
    cout << "Value changed by reference to 3: " << var << endl;

    return 0;
}
