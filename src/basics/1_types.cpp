/**
 * @file 1_types.cpp
 * @author Anthony Correia (anthony.correia@cern.ch)
 * @brief 
 * @version 0.1
 * @date 2022-12-04
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/**
 * @defgroup basics Basics 
 * @{
 */

/////////////////////////////////////////////////////////////////////////////
/// @defgroup types Types
/// @{
/////////////////////////////////////////////////////////////////////////////

/// @brief Auto keyword to determine type from declaration [C++11]
auto a = 12;

/*********************************************************************
 *  @defgroup bool Booleans
 *  @{
 ********************************************************************/

/// @brief A boolean equal to `true`
bool boolean_true = true;

/// @brief A boolean equal to `false`
bool boolean_false = false;

/// @}
/*********************************************************************
 *  @defgroup int Integers
 *  @{
 ********************************************************************/

/// @brief min 8-bit integer, signed or not (platform-dependent)
char c1 = 'a'; // signed or not
/// @brief min 8-bit signed integer
signed char c2 = 4;
/// @brief min 8-bit unsigned integer
unsigned char uc = 4;

/// @brief min 16-bit signed integer
short int s1 = -444;
/// @brief min 16-bit signed integer (equivalent)
short s2 = -444;
/// @brief min 16-bit unsigned integer
unsigned short us = 444;

/// @brief min 16-bit (usually 32 bits) signed integer
int i = -12;
/// @brief min 16-bit (usually 32 bits) unsigned integer
unsigned int ui = 12;

/// @brief min 32-bit (usually 64 bits) signed integer
long l = 12;

/// @brief min 32-bit (usually 64 bits) signed integer
unsigned long ul = 12;

/// @brief min 64-bit signed integer
long long ll = 12;
/// @brief min 64-bit unsigned integer
unsigned long long ull = 12;

// Portable numeric types
#include <cstdint>
/// @brief Portable 8-bit signed integer
int8_t c8 = -3;
/// @brief Portable 8-bit unsigned integer
uint8_t uc8 = 3;
/// @brief Portable 16-bit signed integer
int16_t c16 = -3;
/// @brief Portable 16-bit unsigned integer
uint16_t uc16 = 3;
/// @brief Portable 32-bit signed integer
int32_t c32 = -3;
/// @brief Portable 32-bit unsigned integer
uint32_t uc32 = 3;
/// @brief Portable 64-bit signed integer
int64_t c64 = -3;
/// @brief Portable 64-bit unsigned integer
uint64_t uc64 = 3;

// integer literals ---------------------------------------
// for basis ..............................................

/// @brief Octal basis literal
int io = 02322; // octal (preceded by 0)
/// @brief Hexadecimal basis literal (preceded by 0x)
int ih1 = 0x4d2;
/// @brief Hexadecimal basis literal (preceded by 0X)
int ih2 = 0X4D2;
/// @brief Binary basis literal (preceded by 0b) [C++14]
int ib = 0b101;

// digit separators [C++14] ...............................
/// @short use of digit separator in decimal basis [C++14]
int isep = 123'523'124;
/// @short use of digit separator in another basis, here binary [C++14]
int ibsep = 0b10'001'110;

// for types ..............................................
///@short signed short literal
short lsi = 42;
///@short unsigned short literal
short lui1 = 42u;
///@short unsigned short literal (equivalent)
short lui2 = 42U;

///@short Signed long literal
long lsl1 = 42l;
///@short Signed long literal (equivalent)
long lsl2 = 42L;
///@short Unsigned long literal
long lul1 = 42ul;
///@short Unsigned long literal (equivalent)
long lul2 = 42UL;

///@short Signed long long literal
long long lsll1 = 42ll;
///@short Signed long long literal (equivalent)
long long lsll2 = 42LL;
///@short Unsigned long long literal
long long lull1 = 42ull;
///@short Unsigned long long literal (equivalent)
long long lull2 = 42ULL;

/// @}
/*********************************************************************
 *  @defgroup string Strings
 *  @{
 ********************************************************************/
/// @short Array of chars ended by `\0`, representing a string
const char* s = "A C string";

#include <iostream>
/// @short C++ String. Provided by the STL library
std::string t = "A C++ string"; 

/// @}
/*********************************************************************
 *  @defgroup float Floating number
 *  @{
 ********************************************************************/

/// @short 32 bit = 23 (mantissa) + 8 (exponent) + 1 (sign)
float f = 1.23;
/// @short 64 bit = 52 (mantissa) + 11 (exponent) + 1 (sign)
double d = 1.23;
/// @short change in different platforms (e.g., 80 bits)
long double ld = 1.23;

// Floating-point literals -------------------------------
// Separators ............................................
/// @short Double defined using a point without the 0
double d1 = 12.;
/// @short Double defined using `e`, standing for \f$\times 10^{\bullet}\f$
double de1 = 12.2e34;
/// @short Double defined using `E`, standing for \f$\times 10^{\bullet}\f$
double de2 = 12.2E34;
/// @short Digit separator used in floating numbers [C++14]
double ds = 123'456.876'12;
// Basis .................................................
/// @short Use of different basis to define a double [C++14]
/// Stands for \f$ 0x4d2 \times 10^{6} \times 2^{3} \f$
double db = 0x4d2.1E6p3; // 0x4d2 * 10^6 * 2^3
// Types .................................................
/// @short Float literal
float tf1 = 3.14f;
/// @short Float literal (equivalent)
float tf2 = 3.14F;
/// @short Double literal
double td1 = 3.14;
/// @short Long double literal
long double tld1 = 3.14l;
/// @short Long double literal (equivalent)
long double tld2 = 3.14L;

/// @}
/*********************************************************************
 *  @defgroup other Other types
 *  @{
 ********************************************************************/
#include <cstddef>

/// @short Provided by `<strddef>`. Size of any type.
size_t size = sizeof(double);

void cstddef_header() {
    std::cout << "Size of a double: " << size << std::endl;
}
/// @}

/*********************************************************************
 *  @defgroup custom Custom types
 *  @{
 ********************************************************************/

/// @short Define a type using `typedef` [C++98]
typedef uint64_t specialint1;
/// @short Define a variable of a custom type (for `typedef`)
specialint1 special_var1 = 17;

/// @short Define a type using `using` [C++11]
using specialint2 = uint64_t;
/// @short Define a variable of a custom type (for `using`)
specialint2 special_var2 = 17;

void typedefs_and_usings() {
    std::cout << "Special int 1: " << special_var1 << std::endl;
    std::cout << "Special int 2: " << special_var2 << std::endl;
}


/// @}
/// @}
/// @}

int main () {
    cstddef_header();
    typedefs_and_usings();
    return 0;
}
