/**
 * @file 9_assertions.cpp
 * @author Anthony Correia (anthony.correia@cern.ch)
 * @brief 
 * @version 0.1
 * @date 2022-12-04
 * 
 * 
 */

#include <iostream>

/** @addtogroup basics 
 *  @{
 */

/////////////////////////////////////////////////////////////////////////////
/// @defgroup assertions Assertions
/// Allow to check an invariant. \n
/// An invariant is a property that is guaranteed to be true during certain
/// phases of the program, or the program might crash or yield wrong results. \n
/// The program will abort if an assertion fails \n
/// Assertions can be disabled with `-DNDEBUG`, as it can impact the speed
/// of the program.
/// @{
/////////////////////////////////////////////////////////////////////////////
// Necessary inclusion
#include <cassert>

int a = 0;

/// @brief Assert that `a` is 0
/// @param a 
void assert_example(float a = 0.) {
    assert(a == 0.);
    std::cout << "We asserted that a is 0" << std::endl;
}

// Some custom type
using usertype = float;

/// @brief Static assertion.
/// 
/// Check invariants at compile time. NO runtime penalty!
/// @param a 
void static_assert_example(usertype a = 0.) {
    static_assert(
        std::is_floating_point<usertype>::value,
        "This function except floating-point types."
    );
    std::cout << "usertype is float" << std::endl;
}


int main() {
    assert_example();
    static_assert_example();
    return 0;
}