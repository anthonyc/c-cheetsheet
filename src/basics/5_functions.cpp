/**
 * @file 5_functions.cpp
 * @author Anthony Correia (anthony.correia@cern.ch)
 * @brief 
 * @version 0.1
 * @date 2022-12-04
 * 
 * 
 */
#include <iostream>
using namespace std;

/** @addtogroup basics 
 *  @{
 */

/////////////////////////////////////////////////////////////////////////////
/// @defgroup func Functions
/// @{
/////////////////////////////////////////////////////////////////////////////

/*********************************************************************
 *  @defgroup def Definition
 *  @{
 ********************************************************************/


/** 
 * @brief Add two integers
 * 
 * By default, pararameters of a function are passed by value.
 * This means that they are COPIED!
 * This should be avoided, especially for large variables.
 * @param a first integer
 * @param b second integer
 * @return a + b
*/
int add(
    int a,
    int b = 2 // default argument
              // (must be the training argument)
) {
    return a + b;
}

void definition() {
    cout << "1 + 2 = " << add(1) << endl;
}

/** @} */

/*********************************************************************
 *  @defgroup ref Parameter passed by reference
 *  @{
 ********************************************************************/

/** 
 * @brief Add one to an integer
 * 
 * `var` is passed by reference so that there is no copy.
 * This is preferable for required parameters.
 * @param var an integer, passed by reference (no copy)
*/
void add_one(int &var) {
    var += 1;
}

/**
 * @brief Print an integer
 * 
 * `var` is passed by reference as a constant to indicate that it cannot
 * be modified.
 * @param var integer
 */
void print_int(const int &var) {
    cout << var << endl;
}


void by_reference() {
    int myvar = 0;
    add_one(myvar);
    cout << "0 + 1 = ";
    print_int(myvar);
}

/** @} */


/*********************************************************************
 *  @defgroup pointer Parameter passed by pointer
 *  @{
 ********************************************************************/

/**
 * @brief Add two to an integer
 * 
 * `var` is passed by pointer so that there is not copy.
 * This is preferable for optional parameters (it allows `nullptr`).
 * @param var an integer, passed by reference
 */
void add_two(int *var) {
    *var += 2;
}

/**
 * @brief Print an integer
 * 
 * `var` is passed by pointer as a constant to indicate that it cannot
 * be modified
 * @param var integer
 */
void print_int_pointer(const int *var) {
    cout << *var << endl;
}


void by_pointer() {
    int myvar = 0;
    add_two(&myvar);
    cout << "0 + 2 = ";
    print_int_pointer(&myvar);
}

/** @} */
/** @} */
/** @} */
int main() {
    definition();
    by_reference();
    by_pointer();

    return 0;
}